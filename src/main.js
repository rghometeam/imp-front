import '@babel/polyfill'
import 'mutationobserver-shim'
import Vue from 'vue'
import './plugins/bootstrap-vue'
import App from './App.vue'
import Router from 'vue-router';
import router from './router';
import axios from 'axios';
import join from 'url-join';

Vue.config.productionTip = false

Vue.use(Router);

const isAbsoluteURLRegex = /^(?:\w+:)\/\//

/**
 * Set base url
 */
axios.interceptors.request.use((config) => {
  // Concatenate base path if not an absolute URL
  if (!isAbsoluteURLRegex.test(config.url)) {
    config.url = join(process.env.VUE_APP_BASE_URI, config.url);
  }

  return config;
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')

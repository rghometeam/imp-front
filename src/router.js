import Vue from 'vue';
import Router from 'vue-router';
import Worker from './components/Worker.vue';
import Import from './components/Import.vue';
import Export from './components/Export.vue';
import Accounts from './components/Accounts.vue';
import Statistic from './components/Statistic.vue';


Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: "/",
      redirect: "/worker",
    },
    {
      path: '/worker',
      name: 'Worker',
      component: Worker,
    },
    {
      path: '/import',
      name: 'Import',
      component: Import,
    },
    {
      path: '/export',
      name: 'Export',
      component: Export,
    },
    {
      path: '/accounts',
      name: 'Accounts',
      component: Accounts,
    },
    {
      path: '/statistic',
      name: 'Statistic',
      component: Statistic,
    },
  ],
});
